package com.glearning.hibernate.client;

import com.glearning.hibernate.model.Teacher;
import com.glearning.hibernate.service.TeacherCRUDService;

import java.util.List;

public class TeacherCRUDClient {
    static TeacherCRUDService  crudService = new TeacherCRUDService();
    public static void main(String[] args) {
        insertRecords();
        updateTeacher(1L);
        deleteTeacher(1L);
    }

    private static void insertRecords(){
        Teacher ramesh = new Teacher("Ramesh", 25000, "ramesh@gmail.com");
        Teacher suresh = new Teacher("Suresh", 35000, "suresh@gmail.com");
        Teacher ravi = new Teacher("Ravi", 30000, "ravi@gmail.com");
        crudService.insertTeacherRecord(ramesh);
        crudService.insertTeacherRecord(suresh);
        crudService.insertTeacherRecord(ravi);
    }

    private static void fetchTeacherById(long teacherId){
        Teacher fetchedTeacher = crudService.fetchTeacherById(teacherId);
        if (fetchedTeacher != null){
            System.out.println("========Fetching teacher by id ============");
            System.out.println(fetchedTeacher);
            System.out.println("========Fetching teacher by id ============");
        }
    }

    private static void printAllTeachers(){
        System.out.println("Fetching all the teachers :: ");
        List<Teacher> teachers = crudService.fetchAllTeachers();
        System.out.println("==========Fetching all teachers ==============");
        teachers.forEach(teacher -> System.out.println(teacher));
        System.out.println("==========Fetching all teachers ==============");
    }

    private static void updateTeacher(long teacherId){
        System.out.println("Updating the teacher details");
        crudService.updateTeacher(teacherId);
    }
    private static void deleteTeacher(long teacherId){
        System.out.println("Deleting the teacher details");
        crudService.deleteTeacherById(teacherId);
    }
}
