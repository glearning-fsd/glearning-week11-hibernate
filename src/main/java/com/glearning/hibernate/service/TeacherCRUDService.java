package com.glearning.hibernate.service;

import com.glearning.hibernate.model.Teacher;
import com.glearning.hibernate.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.Serializable;
import java.util.List;

public class TeacherCRUDService {

    private final SessionFactory sessionFactory;
    private Session session;

    public TeacherCRUDService(){
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public Teacher insertTeacherRecord(Teacher teacher){
        //save the teacher instance to the database
        try {
            session = sessionFactory.openSession();
            //begin the transaction
            Transaction transaction = session.beginTransaction();
            //save the teacher instance to the database
            Long result = (Long) session.save(teacher);
            System.out.println("Generated identifier");
            transaction.commit();
        } catch ( Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            session.close();
        }
        return teacher;
    }

    public Teacher fetchTeacherById(long teacherId){
        try {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            Teacher teacher = session.get(Teacher.class, teacherId);
            System.out.println(" Inside the ferchedTeacher ById method");
            transaction.commit();
            return teacher;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            {
                session.close();
            }
        }
        return null;
    }

    public List<Teacher> fetchAllTeachers(){
        try {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            List<Teacher> teachers = session.createQuery("from Teacher").list();
            transaction.commit();
            return teachers;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            {
                session.close();
            }
        }
        return null;
    }

    public Teacher updateTeacher(Long teacherId){
        try {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            Teacher teacherFromDB = session.get(Teacher.class, teacherId);
            teacherFromDB.setName("Ramesh Kumar");
            session.saveOrUpdate(teacherFromDB);
            transaction.commit();
            return teacherFromDB;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            {
                session.close();
            }
        }
        return null;
    }
    public void deleteTeacherById(Long teacherId){
        try {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            Teacher teacherFromDB = session.get(Teacher.class, teacherId);
            session.delete(teacherFromDB);
            transaction.commit();
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            {
                session.close();
            }
        }
    }
}
