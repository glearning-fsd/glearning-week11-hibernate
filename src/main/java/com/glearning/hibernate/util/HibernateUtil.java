package com.glearning.hibernate.util;

import com.glearning.hibernate.model.Teacher;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory = null;
    public static final SessionFactory getSessionFactory(){
        if (sessionFactory == null) {
           sessionFactory = new Configuration()
                    .configure("hibernate-cfg.xml")
                    .addAnnotatedClass(Teacher.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
